<? session_start(); ?>
<html>
<head>
    <title><?= $title ?></title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
          integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <link rel="stylesheet" href="/assets/css/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"
            integrity="sha256-Qw82+bXyGq6MydymqBxNPYTaUXXq7c8v3CwiYwLLNXU="
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>

    <?php
    if (isset($scripts)):
        foreach ($scripts as $script):
            ?>
            <script src="/assets/js/<?= $script ?>.js"></script>
            <?php
        endforeach;
    endif;
    ?>
</head>
<body>
<?php
include "menu.php";
$authPage = array("actor", "client", "film", "genre", "producer", "user", "rental", "rental_history", "black_list");
if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/views/$body.php")) {

    if(in_array($body, $authPage)){
        if(!((isset($_COOKIE["token"]) && isset($_SESSION["user_id"])) && $_COOKIE["token"] === $_SESSION["token"])){
            die("ERROR");
        }
    }
    require_once($_SERVER['DOCUMENT_ROOT'] . "/views/$body.php");

}
?>
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit title</h5>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="saveChanges">Save changes</button>
            </div>
        </div>
    </div>
</div>
</body>
<html>