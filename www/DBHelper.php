<?php

include_once("Log.php");
include_once("config.php");
class DBHelper
{


    private $link = null;

    public function __construct()
    {
        $this->link = mysqli_connect(HOST, USER, PASSWORD, DATABASE)
        or die("CONNECT ERROR: " . mysqli_error($this->link));
    }

    public function __destruct()
    {
        mysqli_close($this->link);
    }

    public function select($table, $condition = "1 = 1")
    {
        $sqlQuery = "SELECT * FROM " . $this->link->real_escape_string($table). " WHERE " .
            $this->link->real_escape_string($condition);
        $result =  mysqli_query($this->link, $sqlQuery);
        Log::login("DB.log", $sqlQuery);
        $result = mysqli_fetch_all($result, MYSQLI_ASSOC);
        return $result? $result: array();
    }

    public function update($table, $setInfo, $condition = "1 = 1")
    {
        $setInfoSql = "";
        foreach ($setInfo as $key => $value){
            $setInfoSql .= "`" . $key ."`" . " = " . "\"$value\"" . ", ";
        }
        $setInfoSql = substr($setInfoSql, 0, -2);
        $sqlQuery = "UPDATE " . $this->link->real_escape_string($table). " SET " .
            $setInfoSql . " WHERE " . $this->link->real_escape_string($condition);
        Log::login("DB.log", $sqlQuery);
        if (mysqli_query($this->link, $sqlQuery))
        {
            return true;
        }
        return false;
    }

    public function delete($table, $condition = "1 = 1")
    {
        $sqlQuery = "DELETE FROM " . $this->link->real_escape_string($table). " WHERE " .
            $this->link->real_escape_string($condition);
        Log::login("DB.log", $sqlQuery);
        if (mysqli_query($this->link, $sqlQuery))
        {
            return true;
        }
        return false;
    }

    public function insert($table, $values)
    {
        foreach ($values as $key => $value){
            $insertColumns[] = $key;
            $insertValues[] = "\"" . $value . "\"";
        }

        $sqlQuery = "INSERT INTO " . $this->link->real_escape_string($table).
            " ( ". implode(", " ,$insertColumns) ." ) " . " VALUES (" .
            implode(", " ,$insertValues). ")";
        Log::login("DB.log", $sqlQuery);
        if (mysqli_query($this->link, $sqlQuery))
        {
            $id = mysqli_insert_id($this->link);
            if($id == 0) {
                return false;
            }
            return end($this->select($table, "id = " . $id));
        }
        return false;
    }

    public function query($sqlQuery)
    {
        Log::login("DB.log", $sqlQuery);
        if ($result = mysqli_query($this->link, $sqlQuery))
        {
            return $result;
        }
        return false;
    }

    public function getColumnsNames($table){
        $sqlQuery = "SELECT * FROM " . $table;
        Log::login("DB.log", $sqlQuery);
        $result =  mysqli_fetch_fields(mysqli_query($this->link, $sqlQuery));
        $names = array();
        foreach ($result as $row){
            $names[] = $row->name;
        }
        return $names;
    }


}

?>