<?php
include_once("DBHelper.php");
$title = "Black list";
$body = $table = "black_list";
$db = new DBHelper();
if (!empty($_POST['event'])) {
    $event = $_POST['event'];
    $id = $_POST["id"] ? $_POST["id"] : "";
    unset($_POST["event"]);
    switch ($event) {
        case "update":
            $db->update($table, $_POST, "id = " . $id);
            break;
        case "delete":
            $db->delete($table, "id = " . $id);
            break;
        case "create":
            $items = array($db->insert($table, array("id_client" => $_POST["client"], "reason" => $_POST["reason"], "time" => date("Y-m-d"))));
            include_once $_SERVER['DOCUMENT_ROOT'] . '/views/components/update.php';
            break;
    }
} else {
    $items = $db->select($table);
    $clients = $db->select("client", "isBlock = 0");
    $columns = $db->getColumnsNames($table);
    $scripts = array('dataWorker');
    include_once("main.php");
}