$(function () {
    $(document).on("click", "button[id^=update]", function (e) {
        var id = $(this).attr("id").replace(/\D/g, '');
        var data = {};
        $.each(fields, function (index, field) {
            data[field] = $('[id="' + field + '[' + id + ']"').val();
        });
        data['event'] = 'update';
        $.ajax({
            type: "POST",
            data: data,
            success: function (response) {
                console.log(response);
            }
        });
        console.log(data);
    });
    $(container).on("click", "[id^=edit]", function (e) {
        e.preventDefault();
        var id = $(this).attr("id").replace(/\D/g, '');
        $.ajax({
            type: "POST",
            data: {
                id: id,
                event: "get"
            },
            success: function (response) {
                $('#editModal').find('.modal-body').html(response);
                $('#editModal').modal('show');
            }
        });
    });
    $(document).on("click", "button[id^=remove]", function (e) {
        var id = $(this).attr("id").replace(/\D/g, '');
        var data = {
            'event': 'delete',
            'id': id
        };
        $.ajax({
            type: "POST",
            data: data,
            success: function (response) {

                $("[id='update[" + id + "]']").remove();
            }
        });

    });

    $("form").on("submit", function (e) {
        e.preventDefault();
        var data = new FormData(this);
        $.ajax({
            type: "POST",
            data: data,
            processData: false,
            contentType: false,
            success: function (response) {
                $(container).append(response);
            }
        });
        return false;
    });
    $('#editModal').on("submit", "#editForm", function (e) {
        e.preventDefault();
        var data = new FormData(this);
        $.ajax({
            type: "POST",
            data: data,
            processData: false,
            contentType: false,
            success: function (response) {
                // location.reload();
            }
        });
        return false;
    });
    $('#saveChanges').on("click", function (e) {
        $("#editForm").trigger('submit');
    });

});