<form action="<?= basename(__FILE__) ?>" method="POST" class="form-group form-control">
    <? include_once $_SERVER['DOCUMENT_ROOT'] . '/views/components/add.php'; ?>
    <div class="col-12">
        <label for="films">Films</label>
        <select class="form-control" name="films[]" multiple>
            <? foreach ($films as $film): ?>
                <option value="<?= $film['id'] ?>"> <?= $film['name'] ?> </option>
            <? endforeach; ?>
        </select>
    </div>
    <div class="col-12 btn-snd-req">
        <input class="btn btn-lg btn-success btn-block" type="submit" value="Send request">
    </div>
</form>
<div class="col-12 genres">
    <?
    include_once $_SERVER['DOCUMENT_ROOT'] . '/views/components/update.php';
    ?>
</div>
<script>
    var fields = <?=json_encode($columns)?>;
    var container = '.genres';
</script>