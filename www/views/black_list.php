<form action="<?= basename(__FILE__) ?>" method="POST" class="form-group form-control">
    <input type="hidden" name="event" value="create">
    <div class="col-12">
        <label for="client">Clients </label>
        <select name="client" class="form-control">
            <? foreach($clients as $client) : ?>
                <option value="<?=$client['id'] ?>"> <?=$client["name"] . " " . $client["last_name"]?> </option>  
            <?endforeach ?>
        </select>
    </div>
     <div class="col-12">
        <label for="reason">Reason </label>
        <input type="text" name="reason" class="form-control">
    </div>
    <div class="col-12 btn-snd-req">
        <input class="btn btn-lg btn-success btn-block" type="submit" value="Send request">
    </div>
</form>
<div class="col-12 clients">
    <?
    include_once $_SERVER['DOCUMENT_ROOT'] . '/views/components/update.php';
    ?>
</div>
<script>
    var fields = <?=json_encode($columns)?>;
    var container = '.clients';
</script>