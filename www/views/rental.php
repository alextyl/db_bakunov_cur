<form action="<?= basename(__FILE__) ?>" method="POST" class="form-group form-control">
    <input type="hidden" name="event" value="create">
    <div class="row col-12">
        <div class="col-6">
            <label for="client">Clients</label>
            <select class="form-control" name="client">
                <? foreach ($clients as $client): ?>
                    <option value="<?= $client['id'] ?>"> <?= $client['name'] ?> </option>
                <? endforeach; ?>
            </select>
        </div>
        <div class="col-6">
            <label for="film">Films</label>
            <select class="form-control" name="film">
                <? foreach ($films as $film): ?>
                    <option value="<?= $film['id'] ?>"> <?= $film['name'] ?> </option>
                <? endforeach; ?>
            </select>
        </div>
    </div>
    <div class="col-12 btn-snd-req">
        <input class="btn btn-lg btn-success btn-block" type="submit" value="Send request">
    </div>
</form>
<div class="col-12 rental">
    <?
    include_once $_SERVER['DOCUMENT_ROOT'] . '/views/components/update.php';
    ?>
</div>
<script>
    var fields = <?=json_encode($columns)?>;
    var container = '.rental';
</script>