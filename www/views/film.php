<form action="<?= basename(__FILE__) ?>" method="POST" class="form-group form-control">
    <? include_once $_SERVER['DOCUMENT_ROOT'] . '/views/components/add.php'; ?>
    <div class="col-12">
        <label for="actors">Actors</label>
        <select class="form-control" name="actors[]" multiple>
            <? foreach ($actors as $actor): ?>
                <option value="<?= $actor['id'] ?>"> <?= $actor['first_name'] . ' ' . $actor['middle_name'] . ' ' . $actor['last_name'] ?> </option>
            <? endforeach; ?>
        </select>
    </div>
    <div class="col-12">
        <label for="genres">Genres</label>
        <select class="form-control" name="genres[]" multiple>
            <? foreach ($genres as $genre): ?>
                <option value="<?= $genre['id'] ?>"> <?= $genre['name'] ?> </option>
            <? endforeach; ?>
        </select>
    </div>
    <div class="col-12">
        <label for="producers">Producers</label>
        <select class="form-control" name="producers[]" multiple>
            <? foreach ($producers as $producer): ?>
                <option value="<?= $producer['id'] ?>"> <?= $producer['first_name'] . ' ' . $producer['middle_name'] . ' ' . $producer['last_name'] ?> </option>
            <? endforeach; ?>
        </select>
    </div>
    <div class="col-12 btn-snd-req">
        <input class="btn btn-lg btn-success btn-block" type="submit" value="Send request">
    </div>
</form>
<div class="col-12 films">
    <?
    include_once $_SERVER['DOCUMENT_ROOT'] . '/views/components/update.php';
    ?>
</div>
<script>
    var fields = <?=json_encode($columns)?>;
    var container = '.films';
</script>