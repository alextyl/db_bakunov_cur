<form method="POST" class="form-group form-control" id="editForm">
    <input type="hidden" value="update" name="event">
    <? foreach ($columns as $column): ?>
        <div class="col-12">
            <label for="<?= $column ?>"><?= ucwords(str_replace("_", " ", $column)) ?></label>
            <input class="form-control" type="text" name="<?= $column ?>" value="<?=$item[$column]?>">
        </div>
    <? endforeach; ?>
    <? if (isset($selectItems)): ?>
        <? foreach ($selectItems as $name => $optionItems) : ?>
            <? include $_SERVER["DOCUMENT_ROOT"] . "/views/components/select.php"; ?>
        <? endforeach; ?>
    <? endif; ?>
</form>
