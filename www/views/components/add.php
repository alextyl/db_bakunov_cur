<input type="hidden" value="create" name="event">
<? foreach ($columns as $column): ?>
    <div class="col-12">
        <label for="<?= $column ?>"><?= ucwords(str_replace("_", " ", $column)) ?></label>
        <input class="form-control" type="text" name="<?= $column ?>">
    </div>
<? endforeach; ?>
