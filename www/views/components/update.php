<? foreach ($items as $item): ?>
    <div class="form-group row" id="<?= basename(__FILE__, '.php') ?>[<?= $item["id"] ?>]">
        <? foreach ($item as $column => $itemFragment): ?>
            <? if((isset($hiddenFields) && !in_array($column,$hiddenFields)) || !isset($hiddenFields)): ?>
            <div class="col-2">
                <label class="label label-info"><?= ucwords(str_replace("_", " ", $column)); ?></label>
                <input type="text" value="<?= $itemFragment ?>" id="<?= $column . "[" . $item['id'] . "]" ?>"
                       class="form-control">
            </div>
            <? endif; ?>
        <? endforeach; ?>
        <div class="col-1 button-container">
            <button id="remove[<?= $item["id"] ?>]" class="btn btn-danger btn-block">&#xD7;</button>
        </div>
        <?if($body != 'rental'): ?>
            <div class="col-1 button-container">
                <button id="edit[<?= $item["id"] ?>]" class="btn btn-primary btn-block">Edit</button>
            </div>
        <? endif;?>
    </div>
<?php endforeach; ?>