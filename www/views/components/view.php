<? foreach ($items as $item): ?>
    <div class="form-group row" id="<?= basename(__FILE__, '.php') ?>[<?= $item["id"] ?>]">
        <? foreach ($item as $column => $itemFragment): ?>
            <? if((isset($hiddenFields) && !in_array($column,$hiddenFields)) || !isset($hiddenFields)): ?>
                <div class="col-2">
                    <label class="label label-info"><?= ucwords(str_replace("_", " ", $column)); ?></label>
                    <input type="text" value="<?= $itemFragment ?>" id="<?= $column . "[" . $item['id'] . "]" ?>"
                           class="form-control">
                </div>
            <? endif; ?>
        <? endforeach; ?>
    </div>
<?php endforeach; ?>