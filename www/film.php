<?php
include_once("DBHelper.php");
$title = "Film";
$body = $table = "film";
$db = new DBHelper();
if (!empty($_POST['event'])) {
    $event = $_POST['event'];
    $id = $_POST["id"] ? $_POST["id"] : "";
    unset($_POST["event"]);
    switch ($event) {
        case "update":
            $db->update($table, $_POST, "id = " . $id);
            break;
        case "delete":
            $db->delete($table, "id = " . $id);
            break;
        case "create":
            $actors = $_POST['actors'];
            unset($_POST['actors']);
            $genres = $_POST['genres'];
            unset($_POST['genres']);
            $producers = $_POST['producers'];
            unset($_POST['producers']);
            $items = array($db->insert($table, $_POST));
            foreach ($actors as $actor) {
                $insertArray["id_actor"] = $actor;
                $insertArray["id_film"] = $items[0]["id"];
                $db->insert("actors_in_film", $insertArray);
                unset($insertArray);
            }
            foreach ($producers as $producer) {
                $insertArray["id_producer"] = $producer;
                $insertArray["id_film"] = $items[0]["id"];
                $db->insert("producers_in_film", $insertArray);
                unset($insertArray);
            }
            foreach ($genres as $genre) {
                $insertArray["id_genre"] = $genre;
                $insertArray["id_film"] = $items[0]["id"];
                $db->insert("genre_in_films", $insertArray);
                unset($insertArray);
            }
            include_once $_SERVER['DOCUMENT_ROOT'] . '/views/components/update.php';
            break;
        case "get":
            $item = end($db->select($table, "id = " . $id));
            $filmActors = $db->select("actor_film", "film_id =  " . $id);
            $seletedActorIds = getIds($filmActors, "actor_id");
            $filmGenres = $db->select("genre_film", "film_id =  " . $id);
            $seletedGenresIds = getIds($filmGenres);
            $filmProducers = $db->select("producer_film", "film_id =  " . $id);
            $seletedProducersIds = getIds($filmProducers);
            $actors = $db->select("actor");
            $genres = $db->select("genre");
            $producers = $db->select("producer");
            $columns = $db->getColumnsNames($table);
            $selectItems = array();
            foreach ($actors as $actor) {
                $selectItems["actors"][] = array("id" => $actor["id"],
                    "value" => $actor['first_name'] . " " . $actor["last_name"],
                    "isSelect" => in_array($actor["id"], $seletedActorIds));
            }
            foreach ($genres as $genre) {
                $selectItems["genres"][] = array("id" => $genre["id"],
                    "value" => $genre['name'],
                    "isSelect" => in_array($genre["id"], $seletedGenresIds));
            }
            foreach ($producers as $producer) {
                $selectItems["producers"][] = array("id" => $producer["id"],
                    "value" => $producer['first_name'] . " " . $producer["last_name"],
                    "isSelect" => in_array($producer["id"], $seletedProducersIds));
            }
            include_once $_SERVER['DOCUMENT_ROOT'] . "/views/components/edit.php";
            break;
    }
} else {
    $items = $db->select($table);
    $actors = $db->select("actor");
    $genres = $db->select("genre");
    $producers = $db->select("producer");
    $hiddenFields = array('id', 'id_video');
    $columns = $db->getColumnsNames($table);
    $scripts = array('dataWorker');
    include_once("main.php");
}
function getIds($array, $field = "id")
{
    $arr = array();
    foreach ($array as $item) {
        $arr[] = $item[$field];
    }
    return $arr;
}