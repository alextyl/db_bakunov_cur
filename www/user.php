<?php
include_once("DBHelper.php");
$title = "User";
$body = $table = "user";
$db = new DBHelper();
if (!empty($_POST['event'])) {
    $event = $_POST['event'];
    $id = $_POST["id"] ? $_POST["id"] : "";
    unset($_POST["event"]);
    switch ($event) {
        case "update":
            $db->update($table, $_POST, "id = " . $id);
            break;
        case "delete":
            $db->delete($table, "id = " . $id);
            break;
        case "create":
            $items = array($db->insert($table, $_POST));
            include_once $_SERVER['DOCUMENT_ROOT'] . '/views/components/update.php';
            break;
    }
} else {
    $items = $db->select($table);
    $columns = $db->getColumnsNames($table);
    $scripts = array('dataWorker');
    include_once("main.php");
}