<?php
include_once("DBHelper.php");
$title = "Rental";
$body = $table = "rental_view";
$db = new DBHelper();
if (!empty($_POST['event'])) {
    $event = $_POST['event'];
    $id = $_POST["id"] ? $_POST["id"] : "";
    unset($_POST["event"]);
    switch ($event) {
        case "update":
            $db->update($table, $_POST, "id = " . $id);
            break;
        case "delete":
            $db->delete("rental", "id = " . $id);
            break;
        case "create":
            session_start();
            $film = $_POST['film'];
            $client = $_POST['client'];
            $item = 
                $db->insert("rental", array(
                    "id_film" => $film, 
                    "id_client" => $client, 
                    "id_user" => $_SESSION["user_id"], 
                    "date" => date("Y-d-m")
                )
            );
            $items = $db->select("rental_view", "id = " .  $item["id"]);
            include_once $_SERVER['DOCUMENT_ROOT'] . '/views/components/update.php';
            break;
            case "get":
                $item = end($db->select($table, "id = " . $id));
                $columns = $db->getColumnsNames($table);
                include_once $_SERVER['DOCUMENT_ROOT'] . "/views/components/edit.php";
            break;
    }
} else {
    $clients = $db->select("client");
    $items = $db->select($table);
    $films = $db->select("film", "isRental = 0");
    $columns = $db->getColumnsNames($table);
    $scripts = array('dataWorker');
    $body = "rental";
    include_once("main.php");
}