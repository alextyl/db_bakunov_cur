<?php
class Log
{
    const isLoginGlobal = true;
    public static function login($file, $logText, $isLogin = false){
        if(isLoginGlobal || $isLogin){
            if(!file_exists($_SERVER['DOCUMENT_ROOT'] . "/logs")) {
                mkdir($_SERVER['DOCUMENT_ROOT'] . "/logs");
            }
            if (!file_exists($file)){
                fopen($file, "w");
            }
            file_put_contents($_SERVER['DOCUMENT_ROOT'] . "/logs/" .$file, date('d.m.Y H:i:s') . ": " . $logText . "\n", FILE_APPEND);
        }
    }
}
?>