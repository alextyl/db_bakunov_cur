<?php
include_once("DBHelper.php");
$title = "Producer";
$body = $table = "producer";
$db = new DBHelper();
if (!empty($_POST['event'])) {
    $event = $_POST['event'];
    $id = $_POST["id"] ? $_POST["id"] : "";
    unset($_POST["event"]);
    switch ($event) {
        case "update":
            $db->update($table, $_POST, "id = " . $id);
            break;
        case "delete":
            $db->delete($table, "id = " . $id);
            break;
        case "create":
            $films = $_POST['films'];
            unset($_POST['films']);
            $items = array($db->insert($table, $_POST));
            foreach ($films as $film){
                $insertArray["id_film"] = $film;
                $insertArray["id_producer"] = $items[0]["id"];
                $db->insert("producers_in_film", $insertArray);
            }
            include_once $_SERVER['DOCUMENT_ROOT'] . '/views/components/update.php';
            break;
                       case "get":
                $item = end($db->select($table, "id = " . $id));
                $columns = $db->getColumnsNames($table);
                include_once $_SERVER['DOCUMENT_ROOT'] . "/views/components/edit.php";
            break;
    }
} else {
    $items = $db->select($table);
    $films = $db->select("film");
    $columns = $db->getColumnsNames($table);
    $scripts = array('dataWorker');
    include_once("main.php");
}