<?php
include_once("DBHelper.php");
session_start();
Log::login("DB.log", "POST: " . json_encode($_POST));
$db = new DBHelper();
$user = $db->select("user", "name = " . $_POST["login"] . " and id_number = " . $_POST["id_number"]);
if(!empty($user)){
    $token = uniqid('', true);
    $_SESSION["token"] = $token;
    $_SESSION["user_id"] = $user[0]["id"];
    setcookie("token", $token);
    die(json_encode(array("isSuccess" => true, "token" => $token)));
} else {
    die(json_encode(array("isSuccess" => false)));
}

?>